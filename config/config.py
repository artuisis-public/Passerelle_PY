# ================== Passerelle Parameters ================== #
# !! --------------- If some variable are modified !! ------- #
#    Use the same format : var_name = var_value
#    (space between and after the equal are important)
# !! -------------------------------------------------------- #
# Variable for the simulation : (True : On ; False : Off)
simState = True

# ----------------- Simulation ----------------- #
# IP Adress for the robots :
IP_Address_s = "192.168.56.1"

# IP adress and port for the supervisor :
IP_Adress_Supervisor = "192.168.56.2"
SupervisorPort = 20001

# ----------------- Real Robots ----------------- #
# IP Adress for the router :
IP_Address_r = "192.168.8.142"

# Optitrack IP Adress and parameters :
IP_Optitrack = "192.168.8.155"

# ----------------- Options ----------------- #
# Frequency :
Frequency = 5 # In Hz

# Port for the robots
Port = 10001

# Buffer size :
Buffer_size = 10000

# Robots Parameters :
fieldOfViewSize = 1 # In meters
blindSpotSize = 180 # In degree
# ------------------------------------------- #
