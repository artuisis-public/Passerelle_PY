# ==================================================================================================================== #
#                                                 IMPORTS
# ==================================================================================================================== #
# import libraries
import tkinter as tk
import tkinter.ttk as ttk
import struct
# Import scripts
import config.config as config


# ==================================================================================================================== #
#                                                  VARIABLES
# ==================================================================================================================== #
# Robot state (On/Off) :
state = False
init = False

# Command message for the robots :
message = bytearray(struct.pack("B", 0))

# Variable set
fieldOfViewSize = config.fieldOfViewSize
blindSpotSize = config.blindSpotSize

# Rules list with intensity
rules = {
    "Friction": 0.1,
    "Attraction": 3.0,
    "Repulsion": 0.5,
    "Alignment": 2.0,
    "RandomMovement": 0.0,
    "CollisionAvoidance": 20.0,
    "AvoidObstacles": -20.0,
}


# ==================================================================================================================== #
#                                                   CLASSES
# ==================================================================================================================== #
class Window(tk.Tk):
    def __init__(self, master, rbtServ, argServer):
        self.master = master

        # Frames
        config = Config(self.master, rbtServ, argServer)
        robots = Robots(self.master)
        dataviz = Dataviz(self.master, rbtServ, argServer)

        # Pack
        config.grid(row=0, column=0, sticky='nsew', padx=5, pady=5)
        robots.grid(row=0, column=1, sticky='nsew', padx=5, pady=5)
        dataviz.grid(row=0, column=2, sticky='nsew', padx=5, pady=5)


# -------------------------------------------------------------------------------------------------------------------- #
class Config(tk.Frame):
    def __init__(self, parent, rbtServ, argServer):
        tk.Frame.__init__(self, parent)
        self.rbtServ = rbtServ
        self.argServer = argServer

        # Save the current Config.py in oldConfig.py :
        with open("config/config.py") as f:
            content = f.read()
        # Save it as oldCongfig.py
        f = open("config/oldConfig.py", "w")
        f.write(content)
        f.close()

        # Variables from Config.py
        self.state = config.simState
        self.rip = config.IP_Address_r
        self.sip = config.IP_Address_s
        self.ssup = config.IP_Adress_Supervisor
        self.opt = config.IP_Optitrack
        self.rport = config.Port
        self.sport = config.SupervisorPort
        self.buffer = config.Buffer_size
        self.frequency = config.Frequency

        # Text variables
        self.tvState = tk.StringVar()
        self.tvState.set(str(self.state))
        self.tvServRobots = tk.StringVar()
        self.tvServRobots.set(self.rbtServ.msgServ)
        self.tvServSup = tk.StringVar()
        # self.tvServSup.set(main.spvServ.msgServ)
        self.tvServSup.set(self.argServer.msgServ)

        # Labels creation
        l_title = tk.Label(self, text="Configuration Passerelle", font=("Verdana", 18), relief="solid")
        l_simulation = tk.Label(self, text="Simulation")
        l_labstate = tk.Label(self, text="State")
        self.l_state = tk.Label(self, textvariable=self.tvState)
        if self.state:
            self.l_state.config(fg='green')
        else:
            self.l_state.config(fg='red')
        l_ipadress_real = tk.Label(self, text="IP adress for real robots")
        l_ipadress_optitrack = tk.Label(self, text=" IP adress for Optitrack")
        l_ipadress_simu = tk.Label(self, text="IP adress for simulation")
        l_ipadress_supervisor = tk.Label(self, text="IP for the supervisor")
        l_port_real = tk.Label(self, text="Port for robots")
        l_port_simu = tk.Label(self, text="Port for the Supervisor")
        l_buffer_size = tk.Label(self, text="Buffer size")
        l_frequency = tk.Label(self, text="Frequency")

        # Variable labels
        l_serverStateRobots = tk.Label(self, textvariable=self.tvServRobots, fg='blue')
        l_serverStateSup = tk.Label(self, textvariable=self.tvServSup, fg='blue')

        # Buttons creation
        b_simu = tk.Button(self, text="Simulation State", command=self.simulation)
        b_modification = tk.Button(self, text="Apply modification", command=self.modifConfig)
        b_start = tk.Button(self, text="Start Server", command=self.launchServ)
        b_stop = tk.Button(self, text="Stop Server", command=self.closeServ)

        # Entry creation
        self.e_rip = tk.Entry(self)
        self.e_sip = tk.Entry(self)
        self.e_ssup = tk.Entry(self)
        self.e_opt = tk.Entry(self)
        self.e_rport = tk.Entry(self)
        self.e_sport = tk.Entry(self)
        self.e_buffer = tk.Entry(self)
        self.e_frequency = tk.Entry(self)

        # Entry insert
        self.e_rip.insert(0, self.rip)
        self.e_sip.insert(0, self.sip)
        self.e_ssup.insert(0, self.ssup)
        self.e_opt.insert(0, self.opt)
        self.e_rport.insert(0, self.rport)
        self.e_sport.insert(0, self.sport)
        self.e_buffer.insert(0, self.buffer)
        self.e_frequency.insert(0, self.frequency)

        # Entry config
        if self.state:
            self.e_sip.config(state=tk.NORMAL)
            self.e_ssup.config(state=tk.NORMAL)
            self.e_sport.config(state=tk.NORMAL)
            self.e_rip.config(state=tk.DISABLED)
            self.e_opt.config(state=tk.DISABLED)
        else:
            self.e_rip.config(state=tk.NORMAL)
            self.e_opt.config(state=tk.NORMAL)
            self.e_sip.config(state=tk.DISABLED)
            self.e_ssup.config(state=tk.DISABLED)
            self.e_sport.config(state=tk.DISABLED)

        # Widgets pack
        l_title.grid(row=0, column=0, columnspan=2, sticky='ew')
        l_simulation.grid(row=1, column=0)
        b_simu.grid(column=1, row=1, sticky='ew')
        l_labstate.grid(row=2, column=0)
        self.l_state.grid(row=2, column=1)
        l_ipadress_real.grid(row=3, column=0)
        self.e_rip.grid(row=3, column=1)
        l_ipadress_simu.grid(row=4, column=0)
        self.e_sip.grid(row=4, column=1)
        l_ipadress_optitrack.grid(row=5, column=0)
        self.e_opt.grid(row=5, column=1)
        l_ipadress_supervisor.grid(row=6, column=0)
        self.e_ssup.grid(row=6, column=1)
        l_port_real.grid(row=7, column=0)
        self.e_rport.grid(row=7, column=1)
        l_port_simu.grid(row=8, column=0)
        self.e_sport.grid(row=8, column=1)
        l_buffer_size.grid(row=9, column=0)
        self.e_buffer.grid(row=9, column=1)
        l_frequency.grid(row=10, column=0)
        self.e_frequency.grid(row=10, column=1)
        b_modification.grid(row=11, column=0, columnspan=2, sticky='ew')
        l_serverStateRobots.grid(row=12, column=0, columnspan=2, sticky='ew')
        l_serverStateSup.grid(row=13, column=0, columnspan=2, sticky='ew')
        b_start.grid(row=14, column=0, sticky='ew')
        b_stop.grid(row=14, column=1, sticky='ew')

    def simulation(self):
        if self.state:
            self.state = False
            self.l_state.config(fg='red')
            self.e_rip.config(state=tk.NORMAL)
            self.e_opt.config(state=tk.NORMAL)
            self.e_sip.config(state=tk.DISABLED)
            self.e_ssup.config(state=tk.DISABLED)
            self.e_sport.config(state=tk.DISABLED)
        else:
            self.state = True
            self.l_state.config(fg='green')
            self.e_rip.config(state=tk.DISABLED)
            self.e_opt.config(state=tk.DISABLED)
            self.e_sip.config(state=tk.NORMAL)
            self.e_ssup.config(state=tk.NORMAL)
            self.e_sport.config(state=tk.NORMAL)
        self.tvState.set(str(self.state))

    def modifConfig(self):
        # Open the config file
        # save the old variables
        oldRip = self.rip
        oldSip = self.sip
        oldSsup = self.ssup
        oldOpt = self.opt
        oldRport = self.rport
        oldSport = self.sport
        oldBuffer = self.buffer
        oldFrequency = self.frequency
        # get the new variables
        self.rip = self.e_rip.get()
        self.sip = self.e_sip.get()
        self.ssup = self.e_ssup.get()
        self.opt = self.e_opt.get()
        self.rport = self.e_rport.get()
        self.sport = self.e_sport.get()
        self.buffer = self.e_buffer.get()
        self.frequency = self.e_frequency.get()
        # Read the Config.py file
        f = open("config/Config.py", "r")
        content = f.read()
        f.close()
        # Change variables
        content = content.replace(f"simState = {not self.state}", f"simState = {self.state}")
        content = content.replace(f"IP_Address_r = \"{oldRip}\"", f"IP_Address_r = \"{self.rip}\"")
        content = content.replace(f"IP_Address_s = \"{oldSip}\"", f"IP_Address_s = \"{self.sip}\"")
        content = content.replace(f"IP_Adress_Supervisor = \"{oldSsup}\"", f"IP_Adress_Supervisor = \"{self.ssup}\"")
        content = content.replace(f"IP_Optitrack = \"{oldOpt}\"", f"IP_Optitrack = \"{self.opt}\"")
        content = content.replace(f"Port = {str(oldRport)}", f"Port = {str(self.rport)}")
        content = content.replace(f"Port = {str(oldSport)}", f"Port = {str(self.sport)}")
        content = content.replace(f"Buffer_size = {str(oldBuffer)}", f"Buffer_size = {str(self.buffer)}")
        content = content.replace(f"Frequency = {str(oldFrequency)}", f"Frequency = {str(self.frequency)}")
        # Write the new config in the config.py file
        f = open("config/Config.py", "w")
        f.write(content)
        f.close()

    def launchServ(self):
        # If we are in simulation
        if self.state:
            self.rbtServ.startServer(self.sip, self.rport)
            self.argServer.startServer(self.ssup, self.sport)
        # If we are with real robots
        else:
            self.rbtServ.startServer(self.rip, self.rport)
            self.argServer.collectOptitrackData(self.rip, self.opt)

        # Messages
        self.tvServRobots.set(self.rbtServ.msgServ)
        self.tvServSup.set(self.argServer.msgServ)

    def closeServ(self):
        # Close servers
        self.rbtServ.closeServer()
        if self.state:
            self.argServer.closeServer()
        else:
            self.argServer.closeOptitrack()
        # Messages
        self.tvServRobots.set(self.rbtServ.msgServ)
        self.tvServSup.set(self.argServer.msgServ)


# -------------------------------------------------------------------------------------------------------------------- #
class Robots(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)

        # Text variable
        self.tv_state = tk.StringVar()
        self.tv_state.set("Off")
        self.tv_rule = tk.StringVar()

        # Labels creation
        l_title = tk.Label(self, text="Robots configuration", font=("Verdana", 18), relief="solid")
        l_onoff = tk.Label(self, text="Robots On/Off")
        l_labstate = tk.Label(self, text="State")
        l_fieldview = tk.Label(self, text="Robots field view (in m)")
        l_blindspot = tk.Label(self, text="Robots blind spot (in °)")
        self.l_state = tk.Label(self, textvariable=self.tv_state, fg='red')
        self.l_rule = tk.Label(self, textvariable=self.tv_rule, fg='blue')

        # Button creation
        b_onoff = tk.Button(self, text="On / OFF", command=self.robotsOnOff)
        b_editRules = tk.Button(self, text="Apply robot parameters", command=self.editRules)
        b_init = tk.Button(self, text="Initialisation", command=self.robotInit)

        # Entry creation
        self.e_fieldview = tk.Entry(self)
        self.e_fieldview.insert(0, config.fieldOfViewSize)
        self.e_blindspot = tk.Entry(self)
        self.e_blindspot.insert(0, config.blindSpotSize)
        self.e_rule = tk.Entry(self)

        # Treeview creation
        self.tw_rules = ttk.Treeview(self, columns=("rules", "intensity"), show="headings")
        self.tw_rules.column("# 1", anchor=tk.CENTER)
        self.tw_rules.heading('rules', text='Rules', anchor=tk.CENTER)
        self.tw_rules.column("# 2", anchor=tk.CENTER)
        self.tw_rules.heading('intensity', text='Intensity', anchor=tk.CENTER)
        for key in rules:
            self.tw_rules.insert('', 'end', values=(key, rules[key]))

        # Widgets pack
        l_title.grid(row=0, column=0, columnspan=2, sticky='nsew')
        l_onoff.grid(row=1, column=0, sticky='nsew')
        l_labstate.grid(row=2, column=0, sticky='nsew')
        l_fieldview.grid(row=3, column=0, sticky='nsew')
        l_blindspot.grid(row=4, column=0, sticky='nsew')

        b_onoff.grid(row=1, column=1, sticky='nsew')
        self.l_state.grid(row=2, column=1, sticky='nsew')
        self.e_fieldview.grid(row=3, column=1, sticky='nsew')
        self.e_blindspot.grid(row=4, column=1, sticky='nsew')
        self.tw_rules.grid(row=5, column=0, columnspan=2, sticky='nsew')
        self.l_rule.grid(row=6, column=0, sticky='nsew')
        self.e_rule.grid(row=6, column=1, sticky='nsew')
        b_editRules.grid(row=7, column=0, columnspan=2, sticky='nsew')
        b_init.grid(row=8, column=0, columnspan=2, sticky='nsew')

        # Bind
        self.selectedRule = None
        self.tw_rules.bind("<Double-1>", self.onClick)

    def robotsOnOff(self):
        global message, state
        if state:
            self.tv_state.set("Off")
            state = False
            message = bytearray(struct.pack("B", 0))
            self.l_state.config(fg='red')
        else:
            state = True
            message = bytearray(struct.pack("B", 1))
            self.tv_state.set("On")
            self.l_state.config(fg='green')

    def robotInit(self):
        global message, init, state
        if init:
            init = False
            if state:
                message = bytearray(struct.pack("B", 1))
            else :
                message = bytearray(struct.pack("B", 0))
        else:
            init = True
            message = bytearray(struct.pack("B", 2))

    def onClick(self, event):
        item = self.tw_rules.identify('item', event.x, event.y)
        tuple = self.tw_rules.item(item)
        if tuple['values']:
            self.selectedRule = tuple['values'][0]
            self.tv_rule.set(tuple['values'][0])
            self.e_rule.delete(0, tk.END)
            self.e_rule.insert(0, tuple['values'][1])

    def editRules(self):
        global fieldOfViewSize, blindSpotSize
        fieldOfViewSize = float(self.e_fieldview.get())
        blindSpotSize = float(self.e_blindspot.get())
        if self.selectedRule:
            rules[self.selectedRule] = float(self.e_rule.get())
            for item in self.tw_rules.get_children():
                self.tw_rules.delete(item)
            for key in rules:
                self.tw_rules.insert('', 'end', values=(key, rules[key]))


# -------------------------------------------------------------------------------------------------------------------- #
class Dataviz(tk.Frame):
    def __init__(self, parent, rbtServ, argServer):
        tk.Frame.__init__(self, parent)
        self.rbtServ = rbtServ
        self.argServer = argServer

        # Variables
        self.oldMsg = ""

        # Labels creation
        l_title = tk.Label(self, text="Data visualization", font=("Verdana", 18), relief="solid")

        # Buttons
        b_clear = tk.Button(self, text="Clear console", command=self.clearConsole)

        # Scrollbar
        sb_y = tk.Scrollbar(self, orient='vertical')

        # Textbox
        self.t_message = tk.Text(self, yscrollcommand=sb_y.set)
        self.t_message.insert(tk.INSERT, self.rbtServ.msgRobots)

        # Widgets configuration
        self.t_message.config(state=tk.NORMAL)
        sb_y.config(command=self.t_message.yview())

        # Widgets pack
        l_title.grid(column=0, row=0, sticky='nsew')
        b_clear.grid(column=0, row=2, sticky='nsew')
        sb_y.grid(column=1, row=1, sticky='nsew')
        self.t_message.grid(column=0, row=1, sticky='nsew')

        # Init the update function
        self.update()

    def clearConsole(self):
        self.rbtServ.msgRobots = ""
        self.t_message.delete(1.0, tk.END)

    def update(self):
        if self.rbtServ.msgRobots != self.oldMsg:
            self.t_message.delete(1.0, tk.END)
            self.t_message.insert(tk.INSERT, self.rbtServ.msgRobots)
            self.oldMsg = self.rbtServ.msgRobots
        self.after(100, self.update)


# ==================================================================================================================== #
#                                                   MAIN
# ==================================================================================================================== #
def gui(rbtServ, argServer):
    # gui root
    root = tk.Tk()
    # screen ratio
    # width = root.winfo_screenwidth()
    # height = root.winfo_screenheight()
    # width = 1200
    # height = 700
    # root parameters
    root.title("GUI ARTUISIS")
    # root.geometry("%dx%d" % (width, height))
    # icon
    icon = tk.PhotoImage(file='logo/icon.png')
    root.iconphoto(False, icon)
    # window
    window = Window(root, rbtServ, argServer)
    # gui main loop
    root.mainloop()
# ==================================================================================================================== #
