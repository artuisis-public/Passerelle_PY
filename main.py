# ==================================================================================================================== #
#                                                 IMPORTS
# ==================================================================================================================== #
# Import libraries
import threading
import time
import struct
import pandas as pd
# Import scripts
import config.config as config
import computation
import robotConnection
import supervisorConnection
import optitrackConnection
import GUI
import guiArtuisis


# ==================================================================================================================== #
#                                                 FUNCTIONS
# ==================================================================================================================== #
def msgHead(guiMsg, rules):
    # Type (char) ; Number of rules (char) :
    message = guiMsg + bytearray(struct.pack("B", len(rules)))
    # Rules intensity (float):
    for key in rules.keys():
        message += bytearray(struct.pack("f", rules[key]))
    # Return
    return message


# -------------------------------------------------------------------------------------------------------------------- #
def msgAddPoint(message, x, z):
    # X relative to the point to reach
    message += bytearray(struct.pack("f", x))
    # Z relative to the point to reach
    message += bytearray(struct.pack("f", z))
    # Return
    return message


# -------------------------------------------------------------------------------------------------------------------- #
def msgAddNeig(message, x, z, angle):
    # X relative (float) :
    message += bytearray(struct.pack("f", x))
    # Z relative (float)
    message += bytearray(struct.pack("f", z))
    # Angle relative (float)
    message += bytearray(struct.pack("f", angle))
    # Return
    return message


# ==================================================================================================================== #
#                                                   MAIN
# ==================================================================================================================== #
if __name__ == '__main__':
    # --------- Robots server and thread --------------- #
    # Robots server
    rbtServ = robotConnection.robotServer()
    # Connected robots
    robotsClient = rbtServ.robotsClient

    # --- Supervisor or Optitrack server and thread ---- #
    # Simulation or real robots variable
    simState = config.simState
    # Robots data
    robotTrckData = {}
    # Argument server for the GUI thread (supervisor if simulation, Optitrack if real robots)
    argServer = None

    # If we are simulating
    if simState:
        # Supervisor server
        spvServ = supervisorConnection.supervisorServer()
        argServer = spvServ
        # Robots data from supervisor
        robotTrckData = spvServ.robotTrckData

    # If we are on real robots
    else:
        # Optitrack server
        optServ = optitrackConnection.optitrackServer()
        argServer = optServ
        # Robots data from Optitrack
        robotTrckData = optServ.robotTrckData

    # --------- GUI thread ----------------------------- #
    # mygui = threading.Thread(target=GUI.gui, args=(rbtServ, argServer, ))
    # mygui.start()
    mygui = threading.Thread(target=guiArtuisis.gui, args=(rbtServ, argServer, ))
    mygui.start()
    # Rules intensity
    # rules = GUI.rules
    rules = guiArtuisis.rules

    # --- Robots data log ------------------------------ #
    # Starting time
    startingTime = time.time()

    # Optitrack file : Filename and header columns
    filenameOpt = '../Log/OptitrackInfo.csv'
    columns = ['Robot ID', 'x', 'y', 'z', 'rx', 'ry', 'rz', 'rw', 'Time']
    # Write header columns on the csv file
    with open(filenameOpt, mode='w', encoding='utf-8') as f:
        f.write(','.join(columns) + '\n')

    # Robots file : Filename and header columns
    filenameRbt = '../Log/RobotsInfo.csv'
    columns = ['Robot ID', 'Order', 'Nb neighbours', 'Force X', 'Force Z', 'Time']
    # Write header columns on the csv file
    with open(filenameRbt, mode='w', encoding='utf-8') as f:
        f.write(','.join(columns) + '\n')

    # --------- Infinite loop -------------------------- #
    while True:
        # --- Field of view and blind-spot maj ------------- #
        # fieldOfViewSize = GUI.fieldOfViewSize
        fieldOfViewSize = guiArtuisis.fieldOfViewSize
        # blindSpotSize = GUI.blindSpotSize
        blindSpotSize = guiArtuisis.blindSpotSize

        # --- Robots data log ------------------------------ #
        # Supervisor or Optitrack data
        if robotTrckData:
            timeTrace = time.time()-startingTime
            df = pd.DataFrame(list(robotTrckData.items()), columns=['Robot ID', 'Data'])
            df[['pos', 'rot']] = df['Data'].apply(lambda x : pd.Series(x))
            df[['x', 'y', 'z']] = df['pos'].apply(lambda x : pd.Series(x))
            df[['rx', 'ry', 'rz', 'rw']] = df['rot'].apply(lambda x : pd.Series(x))
            df.drop(['Data', 'pos', 'rot'], axis=1, inplace=True)
            df['Time'] = timeTrace
            df.to_csv(filenameOpt, encoding='utf-8', mode='a', index=False, header=False)

        # Robots data
        if rbtServ.robotsInfo:
            timeTrace = time.time() - startingTime
            df = pd.DataFrame(list(rbtServ.robotsInfo.items()), columns=['Robot ID', 'Data'])
            df[['Order', 'Nb neighbours', 'Force X', 'Force Z']] = df['Data'].apply(lambda x: pd.Series(x))
            df.drop(['Data'], axis=1, inplace=True)
            df['Time'] = timeTrace
            df.to_csv(filenameRbt, encoding='utf-8', mode='a', index=False, header=False)

        # --- For each robot connected to the Passerelle --- #
        for idrobot in list(robotsClient):
            # --- If the robot is tracked by the supervisor --- #
            if idrobot in robotTrckData.keys():

                # --- Order type and rules --- #
                # Message head construction
                # message = msgHead(GUI.message, rules)
                message = msgHead(guiArtuisis.message, rules)
                # Depending on the GUI's order
                # kind = int.from_bytes(GUI.message, "big")
                kind = int.from_bytes(guiArtuisis.message, "big")
                # If the order is "initialization"
                if kind == 2:
                    x, z = computation.posInit(robotTrckData, idrobot, simState)
                    message = msgAddPoint(message, x, z)

                # --- Neighbours computation --- #
                # Neighbours list
                nbNeighbours, detectedAgents = computation.listNeigh(idrobot, robotTrckData, fieldOfViewSize,
                                                                     blindSpotSize, simState)
                # Add Nb neighbours (uint32) to message
                message += bytearray(struct.pack("I", nbNeighbours))
                # For each neighbour
                for neighbour in detectedAgents:
                    # Compute the relative position and rotation of the neighbour
                    if simState:
                        x, z, angle = computation.posNeigSim(robotTrckData, neighbour, idrobot)
                    else:
                        x, z, angle = computation.posNeigReal(robotTrckData, neighbour, idrobot)
                    # Add it to the message
                    message = msgAddNeig(message, x, z, angle)

                # --- Sending the message to the robot--- #
                rbtServ.sending(robotsClient[idrobot], message)

            # --- If the robot is not tracked by the supervisor or optitrack --- #
            else:
                # --- Print it --- #
                # print(f"This robot is not tracked : ID = ", idrobot, " - IP Address :", {robotsClient[idrobot]})
                # --- Order type and rules --- #
                # message = msgHead(GUI.message, rules)
                message = msgHead(guiArtuisis.message, rules)
                # --- Neighbours computation --- #
                message += bytearray(struct.pack("I", 0))
                # --- Sending the message to the robot--- #
                rbtServ.sending(robotsClient[idrobot], message)

        # ------- Send msg to display (AR helmets) ------- #
        #

        # ----------- Loop Frequency --------------------- #
        time.sleep(1 / config.Frequency)
# ==================================================================================================================== #
