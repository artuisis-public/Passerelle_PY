# ==================================================================================================================== #
#                                                 IMPORTS
# ==================================================================================================================== #
# Import libraries
from NatNetClient import NatNetClient
import sys

# ==================================================================================================================== #
#                                                 CLASS
# ==================================================================================================================== #
class optitrackServer:
    def __init__(self):
        # Streamin client
        self.streaming_client = NatNetClient()

        # Dictionnary where all the robots data will be stored : key = ID and
        # data = [position; rotation] -> [x,y,z] and [x,y,z,w]
        self.robotTrckData = {}

        # String for print messages in the GUI:
        self.msgServ = "Optitrack server Off"

    # This is a callback function that gets connected to the NatNet client.
    # It is called once per rigid body per frame
    def receive_rigid_body_frame(self, new_id, position, rotation):
        # Check if the robot id is correct
        if 0 < new_id < 100:
            self.robotTrckData[new_id] = [position, rotation]
        else:
            # print("New Phantom ID : ", new_id)
            pass

    # Collect Optitrack data :
    def collectOptitrackData(self, ipaddr_client, ipaddr_opt):
        # Optitrack configuration
        optionsDict = {}
        optionsDict["clientAddress"] = ipaddr_client
        optionsDict["serverAddress"] = ipaddr_opt
        optionsDict["use_multicast"] = True

        # Client creation
        self.streaming_client.set_client_address(optionsDict["clientAddress"])
        self.streaming_client.set_server_address(optionsDict["serverAddress"])
        self.streaming_client.set_use_multicast(optionsDict["use_multicast"])

        # Configure the streaming client to call our rigid body handler on the emulator to send data out.
        self.streaming_client.rigid_body_listener = self.receive_rigid_body_frame

        # Run the thread
        is_running = self.streaming_client.run()
        if not is_running:
            print("ERROR: Could not start streaming client.")
            try:
                sys.exit(1)
            except SystemExit:
                print("...")
            finally:
                print("exiting")

        # Message construction
        self.msgServ = "Server up and listening for Optitrack \n"
        self.msgServ += f"IP Address : {ipaddr_opt}"

    def closeOptitrack(self):
        # Close streaming client
        self.streaming_client.shutdown()
        # Message construction
        self.msgServ = "Server Optitrack closed"
# ==================================================================================================================== #
