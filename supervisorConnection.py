# ==================================================================================================================== #
#                                                 IMPORTS
# ==================================================================================================================== #
# Import libraries
import select
import socket
import threading
# Import files
import config.config as config


# ==================================================================================================================== #
#                                                 CLASS
# ==================================================================================================================== #
class supervisorServer:
    def __init__(self):
        # Datagram socket
        self.UDPServerSocket = 0

        # Buffer size
        self.bufferSize = config.Buffer_size

        # Thread for listening
        self.event = threading.Event()
        self.supervisorThread = threading.Thread(target=self.listening, args=(self.event,))

        # Dictionnary where all the robots data will be stored : key = ID and
        # data = [position; rotation] -> [x,y,z] and [x,y,z,w]
        self.robotTrckData = {}

        # String for print messages in the GUI:
        self.msgServ = "Server for supervisor closed"

    def createThread(self):
        self.supervisorThread = threading.Thread(target=self.listening, args=(self.event,))

    # Start the server :
    def startServer(self, ipaddr, port):
        # Bind to address and ip
        self.UDPServerSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.UDPServerSocket.bind((ipaddr, int(port)))
        # Start thread
        self.event.clear()
        self.createThread()
        self.supervisorThread.start()
        # Message construction
        self.msgServ = "Server up and listening for Supervisor \n"
        self.msgServ += f"IP Address : {ipaddr} - Port {port}"

    # Close the server :
    def closeServer(self):
        self.event.set()
        self.supervisorThread.join()
        self.UDPServerSocket.close()
        self.msgServ = "Server for Supervisor closed"

    # Listening to supervisor :
    def listening(self, event):
        while True:
            # Decoding message from supervisor
            self.UDPServerSocket.setblocking(0)
            ready = select.select([self.UDPServerSocket], [], [], 0.1)
            if ready[0]:
                message, address = self.UDPServerSocket.recvfrom(self.bufferSize)
                # Decoding message on a good format
                msg = message.decode("utf-8", "ignore")
                # Construction of supervisor data frame
                d = dict(x.split("=") for x in msg.split(";"))
                for k, v in d.items():
                    position, rotation = v.split("_", maxsplit=2)
                    position = list(map(float, position[1:-1].split(",")))
                    rotation = list(map(float, rotation[1:-1].split(",")))
                    self.robotTrckData[int(k)] = [position, rotation]

            # Stop the listening
            if event.is_set():
                break
# ==================================================================================================================== #
