# ==================================================================================================================== #
#                                                 IMPORTS
# ==================================================================================================================== #
# Import libraries
import ipaddress

import select
import socket
import threading
# Import files
import config.config as config


# ==================================================================================================================== #
#                                                 CLASS
# ==================================================================================================================== #
class robotServer:
    def __init__(self):
        # Datagram socket
        self.UDPServerSocket = None

        # Buffer size
        self.bufferSize = config.Buffer_size

        # Thread for listening
        self.event = threading.Event()
        self.robotThread = None

        # ID and IP Address for robots
        self.robotsClient = {}
        # Information from robots
        # [ ID ; Kind ; Rules 1 .. to 7 ; Nb neighbours ; Force X ; Force Z ]
        self.robotsInfo = {}

        # String for print messages in the GUI
        self.msgServ = "Server for robots closed"
        self.msgRobots = "No robot connected \n"

    def createThread(self):
        self.robotThread = threading.Thread(target=self.listening, args=(self.event,))

    # Start the server :
    def startServer(self, ipaddr, port):
        # Bind to address and ip
        self.UDPServerSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.UDPServerSocket.bind((ipaddr, int(port)))
        # Start thread
        self.event.clear()
        self.createThread()
        self.robotThread.start()
        # Build message
        self.msgServ = "UDP server up and listening for robot connexions \n"
        self.msgServ += f"IP Address : {ipaddr} - Port {port}"

    # Close the server :
    def closeServer(self):
        self.event.set()
        self.robotThread.join()
        self.UDPServerSocket.close()
        self.msgServ = "Server for robots closed"

    # Sending message :
    def sending(self, ipaddr, message):
        if not self.event.is_set():
            self.UDPServerSocket.sendto(message, ipaddr)

    # Listening to new clients and register them
    def listening(self, event):
        while True:
            # Decoding message from robot
            self.UDPServerSocket.setblocking(False)
            ready = select.select([self.UDPServerSocket], [], [], 0.1)
            if ready[0]:
                message, address = self.UDPServerSocket.recvfrom(self.bufferSize)
                msg = message.decode("utf-8", "ignore")
                if len(msg) < 3:
                    # Printing new robot information
                    clientMsg = "New robot connected with ID : {}".format(msg)
                    clientIP = "From IP Address :{}".format(address)
                    self.msgRobots += clientMsg + "\n" + clientIP + "\n"
                    # Robot register
                    self.robotsClient[int(message)] = address
                else:
                    rbInfo = msg.split("_")
                    self.robotsInfo[rbInfo[0]] = rbInfo[1:]

            # Stop the listening
            if event.is_set():
                break
# ==================================================================================================================== #
