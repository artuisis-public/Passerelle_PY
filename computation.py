# ==================================================================================================================== #
#                                                 IMPORTS
# ==================================================================================================================== #
# Import libraries
import math
import numpy as np


# ==================================================================================================================== #
#                                                 FUNCTIONS
# ==================================================================================================================== #
"""
Converts a rotation given in axis-angle representation to a quaternion.
Args:
    axis: A 3-element numpy array specifying the rotation axis.
    angle: The rotation angle in radians.
Returns:
    A 4-element numpy array representing the equivalent quaternion.
"""
def axis_angle_to_quaternion(axis, angle):
    # Normalize the axis vector
    axis = axis / np.linalg.norm(axis)
    # Compute the quaternion components
    qw = np.cos(angle / 2)
    qx = axis[0] * np.sin(angle / 2)
    qy = axis[1] * np.sin(angle / 2)
    qz = axis[2] * np.sin(angle / 2)
    # Return the quaternion as a numpy array
    return qw, qx, qy, qz


# -------------------------------------------------------------------------------------------------------------------- #
"""
Convert a quaternion into euler angles (roll, pitch, yaw)
roll is rotation around x in radians (counterclockwise)
pitch is rotation around y in radians (counterclockwise)
yaw is rotation around z in radians (counterclockwise)
"""
def euler_from_quaternion(x, y, z, w):
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)

    return roll_x, pitch_y, yaw_z  # in radians


# -------------------------------------------------------------------------------------------------------------------- #
"""
Detect if the current agent and a selected agent are neighbour depending
on the field of view and the blind spot 
"""
def Perceive(robotTrckData, idAgent, idPotentialNeighbour, fieldOfViewSize, blindSpotSize, simState):
    # If we are on simulation
    if simState:
        # Check whether the potential neighbour is close enough (at a distance shorter than the perception distance)
        # X computation between current robot and potential neighbour
        diff_x = (robotTrckData[idPotentialNeighbour][0][0] - robotTrckData[idAgent][0][0])
        # Y computation between current robot and potential neighbour
        diff_y = (robotTrckData[idPotentialNeighbour][0][1] - robotTrckData[idAgent][0][1])
        # Z computation between current robot and potential neighbour
        #diff_z = (robotTrckData[idPotentialNeighbour][0][2] - robotTrckData[idAgent][0][2])
        # Euler distance between current robot and potential neighbour
        dist = (diff_x * diff_x) + (diff_y * diff_y)  # + (diff_z * diff_z)
        # If the distance is included into the field of view of the current robot
        if dist < (fieldOfViewSize * fieldOfViewSize):
            return True
            # # Roll, pitch and yaw of the current robot
            # qw, qx, qy, qz = axis_angle_to_quaternion(robotTrckData[idAgent][1][:-1], robotTrckData[idAgent][1][3])
            # roll_x, pitch_y, yaw_z = euler_from_quaternion(qx, qy, qz, qw)
            # # computation of the real neighbour's yaw based on roll and yaw
            # if math.fabs(roll_x) > (math.pi / 2.0) and math.fabs(pitch_y) > (math.pi / 2.0):
            #     yaw_z = (math.copysign(math.pi, yaw_z)) - yaw_z
            # angle = (math.acos(diff_z / math.sqrt(diff_x*diff_x + diff_z*diff_z)) - yaw_z) * (180.0/math.pi)
            # # Check whether the potential neighbour is visible by the current agent (not in the blind spot of the current
            # # agent)
            # if math.fabs(angle) <= 180 - (blindSpotSize / 2):
            #     return True
            # else:
            #     return False
        # If the distance is not included in the field of view of the current robot
        else:
            return False

    #If we are on real situation
    else:
        # Check whether the potential neighbour is close enough (at a distance shorter than the perception distance)
        # X computation between current robot and potential neighbour
        diff_x = (robotTrckData[idPotentialNeighbour][0][0] - robotTrckData[idAgent][0][0])
        # Y computation between current robot and potential neighbour
        #diff_y = (robotTrckData[idPotentialNeighbour][0][1] - robotTrckData[idAgent][0][1])
        # Z computation between current robot and potential neighbour
        diff_z = (robotTrckData[idPotentialNeighbour][0][2] - robotTrckData[idAgent][0][2])
        # Euler distance between current robot and potential neighbour
        dist = (diff_x * diff_x) + (diff_z * diff_z) #+ (diff_y * diff_y)
        # If the distance is included into the field of view of the current robot
        if dist < (fieldOfViewSize * fieldOfViewSize):
            return True
            # # Roll, pitch and yaw of the current robot
            # roll_x, pitch_y, yaw_z = euler_from_quaternion(robotTrckData[idAgent][1][0],
            #                                                robotTrckData[idAgent][1][1],
            #                                                robotTrckData[idAgent][1][2],
            #                                                robotTrckData[idAgent][1][3])
            # # computation of the real neighbour's pitch based on roll and yaw
            # if math.fabs(roll_x) > (math.pi/2.0) and math.fabs(yaw_z) > (math.pi/2.0) :
            #     pitch_y = (math.copysign(math.pi, pitch_y)) - pitch_y
            # angle = (math.acos(diff_y / math.sqrt(diff_x*diff_x + diff_y*diff_y)) - pitch_y) * (180.0/math.pi)
            # # Check whether the potential neighbour is visible by the current agent (not in the blind spot of the current
            # # agent)
            # if math.fabs(angle) <= 180 - (blindSpotSize / 2):
            #     return True
            # else:
            #     return False
        # If the distance is not included in the field of view of the current robot
        else:
            return False


# -------------------------------------------------------------------------------------------------------------------- #
"""
Detect the neighbours for a given agent based on is field of view and blind spot
"""
def GetPerceivedAgents(agent, agentList, fieldOfViewSize, blindSpotSize, simState):
    # Create a list that will store the perceived agent
    detectedAgents = []
    # Compare current agent with each agent
    for id in agentList.keys():
        # Check if the current agent is not compared with itself
        if id != agent:
            # Compute if the current agent perceive the selected agent
            if Perceive(agentList, agent, id, fieldOfViewSize, blindSpotSize, simState):
                detectedAgents.append(id)
        else:
            pass
    # Return the neighbours list
    return detectedAgents


# -------------------------------------------------------------------------------------------------------------------- #
def listNeigh(idrobot, robotTrckData, fieldOfViewSize, blindSpotSize, simState):
    # Get the neighbours of the current agent and their ID
    detectedAgents = GetPerceivedAgents(idrobot, robotTrckData, fieldOfViewSize, blindSpotSize, simState)
    # Number of neighbours
    nbNeighbours = len(detectedAgents)
    # Return
    return nbNeighbours, detectedAgents


# -------------------------------------------------------------------------------------------------------------------- #
def posNeigSim(robotTrckData, neighbour, idrobot):
    # neighbour axis-angle to quaternion
    nqw, nqx, nqy, nqz = axis_angle_to_quaternion(robotTrckData[neighbour][1][:-1], robotTrckData[neighbour][1][3])

    # compute roll, pitch and yaw for optitrack data for the neighbour
    n_roll, n_pitch, n_yaw = euler_from_quaternion(nqx, nqy, nqz, nqw)

    # computation of the real neighbour's yaw based on roll and pitch
    if math.fabs(n_roll) > (math.pi / 2.0) and math.fabs(n_pitch) > (math.pi / 2.0):
        n_yaw = (math.copysign(math.pi, n_yaw)) - n_yaw

    # current robot axis-angle to quaternion
    cqw, cqx, cqy, cqz = axis_angle_to_quaternion(robotTrckData[idrobot][1][:-1], robotTrckData[idrobot][1][3])

    # compute roll, pitch and yaw for optitrack data for the current robot
    c_roll, c_pitch, c_yaw = euler_from_quaternion(cqx, cqy, cqz, cqw)

    # computation of the real current robot yaw based on roll and pitch
    if math.fabs(c_roll) > (math.pi / 2.0) and math.fabs(c_pitch) > (math.pi / 2.0):
        c_yaw = (math.copysign(math.pi, c_yaw)) - c_yaw

    # relative angle between neighbour and current robot (in degree) in current robot referential
    angle = (n_yaw - c_yaw) * (180.0 / math.pi)

    # relative distance between neighbour and current robot (in webots)
    # Y and Z axis are inverted between Optitrack and Webots
    x = robotTrckData[neighbour][0][0] - robotTrckData[idrobot][0][0]
    z = robotTrckData[neighbour][0][1] - robotTrckData[idrobot][0][1]

    # vector rotation to be in the current robot referential (not optitrack)
    x2 = x * math.cos(c_yaw) + z * math.sin(c_yaw)
    z2 = z * math.cos(c_yaw) - x * math.sin(c_yaw)

    # Return
    return x2, z2, angle


# -------------------------------------------------------------------------------------------------------------------- #
def posNeigReal(robotTrckData, neighbour, idrobot):
    # compute roll, pitch and yaw for optitrack data for the neighbour
    n_roll, n_pitch, n_yaw = euler_from_quaternion(robotTrckData[neighbour][1][0],
                                                   robotTrckData[neighbour][1][1],
                                                   robotTrckData[neighbour][1][2],
                                                   robotTrckData[neighbour][1][3])

    # computation of the real neighbour's pitch based on roll and yaw
    if math.fabs(n_roll) > (math.pi / 2.0) and math.fabs(n_yaw) > (math.pi / 2.0):
        n_pitch = (math.copysign(math.pi, n_pitch)) - n_pitch

    # compute roll, pitch and yaw for optitrack data for the current robot
    c_roll, c_pitch, c_yaw = euler_from_quaternion(robotTrckData[idrobot][1][0],
                                                   robotTrckData[idrobot][1][1],
                                                   robotTrckData[idrobot][1][2],
                                                   robotTrckData[idrobot][1][3])

    # computation of the real current robot pitch based on roll and yaw /!\ PB
    if math.fabs(c_roll) > (math.pi / 2.0) and math.fabs(c_yaw) > (math.pi / 2.0):
        c_pitch = (math.copysign(math.pi, c_pitch)) - c_pitch

    # relative angle between neighbour and current robot (in degree) in current robot referential
    angle = (n_pitch - c_pitch) * (180.0 / math.pi)

    # relative distance between neighbour and current robot (in optitrack)
    x = robotTrckData[neighbour][0][0] - robotTrckData[idrobot][0][0]
    z = robotTrckData[neighbour][0][2] - robotTrckData[idrobot][0][2]

    # vector rotation to be in the current robot referential (not optitrack)
    x2 = x * math.cos(c_pitch) - z * math.sin(c_pitch)
    z2 = x * math.sin(c_pitch) + z * math.cos(c_pitch)

    # Return
    return x2, z2, angle


# -------------------------------------------------------------------------------------------------------------------- #
def posInit(robotTrckData, idrobot, simState):
    # Point to reach
    px = (idrobot-1)*0.20
    pz = 0.0

    if simState:
        # current robot axis-angle to quaternion
        cqw, cqx, cqy, cqz = axis_angle_to_quaternion(robotTrckData[idrobot][1][:-1], robotTrckData[idrobot][1][3])

        # compute roll, pitch and yaw for optitrack data for the current robot
        c_roll, c_pitch, c_yaw = euler_from_quaternion(cqx, cqy, cqz, cqw)

        # computation of the real current robot yaw based on roll and pitch
        if math.fabs(c_roll) > (math.pi / 2.0) and math.fabs(c_pitch) > (math.pi / 2.0):
            c_yaw = (math.copysign(math.pi, c_yaw)) - c_yaw

        # Relative distance between the robot and the point to reach
        x = px - robotTrckData[idrobot][0][0]
        z = pz - robotTrckData[idrobot][0][1]

        # vector rotation to be in the current robot referential (not optitrack)
        x2 = x * math.cos(c_yaw) + z * math.sin(c_yaw)
        z2 = z * math.cos(c_yaw) - x * math.sin(c_yaw)

    else:
        # compute roll, pitch and yaw for optitrack data for the current robot
        c_roll, c_pitch, c_yaw = euler_from_quaternion(robotTrckData[idrobot][1][0],
                                                       robotTrckData[idrobot][1][1],
                                                       robotTrckData[idrobot][1][2],
                                                       robotTrckData[idrobot][1][3])

        # computation of the real current robot pitch based on roll and yaw /!\ PB
        if math.fabs(c_roll) > (math.pi / 2.0) and math.fabs(c_yaw) > (math.pi / 2.0):
            c_pitch = (math.copysign(math.pi, c_pitch)) - c_pitch

        # Relative distance between the robot and the point to reach
        x = px - robotTrckData[idrobot][0][0]
        z = pz - robotTrckData[idrobot][0][2]

        # vector rotation to be in the current robot referential (not optitrack)
        x2 = x * math.cos(c_pitch) - z * math.sin(c_pitch)
        z2 = x * math.sin(c_pitch) + z * math.cos(c_pitch)

    # Return
    return x2, z2
# ==================================================================================================================== #
